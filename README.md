# AuthApi

## GraalVm

By following the CI stage `buildGraal` and the sbt task `nativeImage` inside `build.sbt` you can find out how the native
image is created.

Don't be confused by the two dockerfiles. The one on the top level is for building a image where the jar can be compiled
to native code. The one inside `out` is for creating the final image.


As you can see that structure is a bit `unclean` and wip. But it can build the program as a native image :D

### Resources

1. https://medium.com/rahasak/run-scala-applications-with-graalvm-and-docker-a1e67701e935
1. https://www.inner-product.com/posts/serverless-scala-services-with-graalvm/
1. https://http4s.org/v1.0/deployment/

## Needs the following environment Vars

| _Env-Var_        | _Sample_          |
| ------------- |-------------| 
| refreshSecret    | abc | 
| accessSecret     | def      | 
| refreshExpiration | 3600 (are seconds)  | 
| accessExpiration | 60 (are seconds)      | 
| JwtAlgorithm | `HS512`    |