import sbt._
import scala.sys.process._
import java.io.File

val Http4sVersion = "0.21.15"
val CirceVersion = "0.13.0"
val MunitVersion = "0.7.20"
val LogbackVersion = "1.2.3"
val MunitCatsEffectVersion = "0.12.0"

val appName = "authapi"

lazy val root = (project in file("."))
  .settings(
    organization := "com.scalangular",
    name := appName,
    version := "0.1.0",
    scalaVersion := "2.13.4",
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "com.pauldijou" %% "jwt-core" % "4.3.0",
      "io.circe" %% "circe-generic" % CirceVersion,
      "org.scalameta" %% "munit" % MunitVersion % Test,
      "org.typelevel" %% "munit-cats-effect-2" % MunitCatsEffectVersion % Test,
      "ch.qos.logback" % "logback-classic" % LogbackVersion,
      "org.scalameta" %% "svm-subs" % "20.2.0",
      "org.mindrot" % "jbcrypt" % "0.4"
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
    testFrameworks += new TestFramework("munit.Framework")
  )

lazy val nativeImage =
  taskKey[File]("Build a standalone executable using GraalVM Native Image")

nativeImage := {
  import sbt.Keys.streams
  val assemblyFatJar = assembly.value
  val assemblyFatJarPath = assemblyFatJar.getParent
  val assemblyFatJarName = assemblyFatJar.getName
  val outputPath = (baseDirectory.value / "out").getAbsolutePath
  val outputName = appName
  val nativeImageDocker = s"${appName}/graalvm-native-image"

  val cmd = s"""docker run
               | --volume ${assemblyFatJarPath}:/opt/assembly
               | --volume ${outputPath}:/opt/native-image
               | ${nativeImageDocker}
               | --static
               | -H:+ReportExceptionStackTraces --no-fallback --allow-incomplete-classpath
               | --enable-all-security-services
               | -jar /opt/assembly/${assemblyFatJarName}
               | ${outputName}""".stripMargin.filter(_ != '\n')
  // enable-all-security-services is needed for jwt

  val log = streams.value.log
  log.info(s"Building native image from ${assemblyFatJarName}")
  log.debug(cmd)
  val result = (cmd.!(log))

  if (result == 0) file(s"${outputPath}/${outputName}")
  else {
    log.error(s"Native image command failed:\n ${cmd}")
    throw new Exception("Native image command failed")
  }
}