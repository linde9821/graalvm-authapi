package com.scalangular.authapi.services

import cats.data.NonEmptyList
import cats.effect.{ConcurrentEffect, Timer}
import cats.implicits._
import com.scalangular.authapi.jwt.JWTAlgebra
import com.scalangular.authapi.models.{JWTToken, User}
import com.scalangular.authapi.repository.UserRepository
import org.http4s.dsl.Http4sDsl
import org.http4s.util.CaseInsensitiveString
import org.http4s.{Challenge, Headers, HttpRoutes, headers}
import pdi.jwt.JwtAlgorithm

object JWTV1Routes {

  val apiVersion = "v1"

  def jwtRefreshTokenRoutes[F[_] : ConcurrentEffect : Timer](JWTRefreshService: JWTAlgebra[F],
                                                             userRepository: UserRepository[F]): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    HttpRoutes.of[F] {
      case req@POST -> Root / this.apiVersion / "login" =>
        for {
          user <- req.as[User]
          isValid <- userRepository.isValid(user.username, user.password)
          claims <- userRepository.getClaims(user.username)
          refreshSecret <- loadFromEnvironment[F, String]("refreshSecret")()
          algorithm <- loadFromEnvironment[F, JwtAlgorithm]("JwtAlgorithm")(JwtAlgorithm.fromString)
          refreshExpiration <- loadFromEnvironment[F, Long]("refreshExpiration")(_.toLong)
          validation <-
            if (isValid) {
              Created(JWTRefreshService.createToken(algorithm, refreshSecret, refreshExpiration, claims))
            }
            else {
              Unauthorized(headers.`WWW-Authenticate`(NonEmptyList.of(Challenge(scheme = "Bearer", realm =
                "Access to authorize a request"))))
            }
        } yield validation
    }
  }

  /**
   * Loads a environment variable from the systems environment.
   *
   * @throws NoSuchElementException when environment variable is not present
   * @param key  key of the environment variable
   * @param pars optional function which maps the value of variable to the desired type
   *             (per default {{{(value: String) => value.asInstanceOf[A]}}} is used)
   */
  @throws(classOf[NoSuchElementException])
  def loadFromEnvironment[F[_], A](key: String)(pars: String => A = (value: String) => value.asInstanceOf[A])(implicit
                                                                                                              F: ConcurrentEffect[F]): F[A] = {
    F.delay {
      sys.env.get(key).fold(throw new NoSuchElementException(s"Couldn't get $key from environment"))(value => pars
      (value))
    }
  }

  def jwtAccessTokenRoutes[F[_] : ConcurrentEffect : Timer](JWTAccessService: JWTAlgebra[F],
                                                            JWTRefreshService: JWTAlgebra[F])
  : HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._
    HttpRoutes.of[F] {
      case req@POST -> Root / this.apiVersion / "auth" =>
        getAuthTokenFromHeader(req.headers) match {
          case Some(token) =>
            for {
              refreshSecret <- loadFromEnvironment[F, String]("refreshSecret")()
              accessSecret <- loadFromEnvironment[F, String]("accessSecret")()
              accessExpiration <- loadFromEnvironment[F, Long]("accessExpiration")(_.toLong)
              algorithm <- loadFromEnvironment[F, JwtAlgorithm]("JwtAlgorithm")(JwtAlgorithm.fromString)
              isValid <- JWTRefreshService.validate(algorithm, token, refreshSecret)
              validation <-
                if (isValid) {
                  Created(JWTAccessService.createToken(algorithm, accessSecret, accessExpiration, Map.empty))
                } else {
                  Unauthorized(headers.`WWW-Authenticate`(NonEmptyList.of(Challenge(scheme = "Bearer",
                    realm = "Access to generate a access token"))))
                }
            } yield validation

          case None => Unauthorized(headers.`WWW-Authenticate`(NonEmptyList.of(Challenge(scheme = "Bearer", realm =
            "Access to authorize a request"))))
        }

      case req@POST -> Root / this.apiVersion / "validate" =>
        getAuthTokenFromHeader(req.headers) match {
          case Some(token) =>
            for {
              accessSecret <- loadFromEnvironment[F, String]("accessSecret")()
              algorithm <- loadFromEnvironment[F, JwtAlgorithm]("JwtAlgorithm")(JwtAlgorithm.fromString)
              isValid <- JWTAccessService.validate(algorithm, token, accessSecret)
              validation <-
                if (isValid) {
                  Ok()
                } else {
                  Unauthorized(headers.`WWW-Authenticate`(NonEmptyList.of(Challenge(scheme = "Bearer",
                    realm = "Access to authorize a request"))))
                }
            } yield validation

          case None => Unauthorized(headers.`WWW-Authenticate`(NonEmptyList.of(Challenge(scheme = "Bearer", realm =
            "Access to authorize a request"))))
        }
    }
  }

  def getAuthTokenFromHeader(headers: Headers): Option[JWTToken] = {
    headers.get(CaseInsensitiveString("Authorization")).fold(none[JWTToken])(header => JWTToken(header.value
      .substring(7)).some)
  }
}
