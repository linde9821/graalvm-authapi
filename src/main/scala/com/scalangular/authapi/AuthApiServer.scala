package com.scalangular.authapi

import cats.effect.{ConcurrentEffect, Timer}
import cats.implicits._
import com.scalangular.authapi.jwt.JWTAlgebra
import com.scalangular.authapi.repository.UserRepository
import com.scalangular.authapi.services.JWTV1Routes
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.{GZip, Logger}

import scala.concurrent.ExecutionContext.global

object AuthApiServer {

  def stream[F[_] : ConcurrentEffect](implicit T: Timer[F]): Stream[F, Nothing] = {
    for {
      _ <- BlazeClientBuilder[F](global).stream
      accessService = JWTAlgebra.accessInter[F]
      refreshService = JWTAlgebra.refreshInter[F]
      userRepository = UserRepository.testInter[F]

      httpApp = (
        JWTV1Routes.jwtAccessTokenRoutes(accessService, refreshService) <+> JWTV1Routes.jwtRefreshTokenRoutes
        (refreshService, userRepository)
        ).orNotFound

      // With Middlewares in place
      loggedHttpApp = Logger.httpApp(logHeaders = true, logBody = false)(GZip(httpApp))

      exitCode <- BlazeServerBuilder[F](global)
        .bindHttp(8080, "0.0.0.0")
        .withHttpApp(loggedHttpApp)
        .serve
    } yield exitCode
  }.drain
}
