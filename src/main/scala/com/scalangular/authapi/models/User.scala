package com.scalangular.authapi.models

import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final class User(val username: String, val password: String)

object User {
  def apply(str: String, str1: String): User = new User(str, str1)

  implicit val userDecoder: Decoder[User] = deriveDecoder[User]

  implicit def userEntityDecoder[F[_] : Sync]: EntityDecoder[F, User] = jsonOf

  implicit val userEncoder: Encoder[User] = deriveEncoder[User]

  implicit def userEntityEncoder[F[_] : Applicative]: EntityEncoder[F, User] = jsonEncoderOf
}
