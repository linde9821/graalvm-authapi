package com.scalangular.authapi.models

import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final class JWTToken(val token: String) extends AnyVal

object JWTToken {
  implicit val jwtDecoder: Decoder[JWTToken] = deriveDecoder[JWTToken]

  implicit def jwtEntityDecoder[F[_] : Sync]: EntityDecoder[F, JWTToken] = jsonOf

  implicit val jwtEncoder: Encoder[JWTToken] = deriveEncoder[JWTToken]

  implicit def jwtEntityEncoder[F[_] : Applicative]: EntityEncoder[F, JWTToken] = jsonEncoderOf

  def apply(string: String): JWTToken = new JWTToken(string)
}
