package com.scalangular.authapi.jwt

import cats.Applicative
import cats.effect.ConcurrentEffect
import cats.implicits.catsSyntaxApplicativeId
import com.scalangular.authapi.models.JWTToken
import pdi.jwt.algorithms.JwtHmacAlgorithm
import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim}

import java.time.Clock
import scala.util.{Failure, Success}

trait JWTAlgebra[F[_]] {
  def validate(jwtAlgorithm: JwtAlgorithm, token: JWTToken, secret: String)(implicit F: ConcurrentEffect[F])
  : F[Boolean]

  def createToken(jwtAlgorithm: JwtAlgorithm, secretKey: String, expirationInSeconds: Long, claims: Map[String,
    String])(implicit F: ConcurrentEffect[F]): F[String]
}

object JWTAlgebra {
  def accessInter[F[_] : Applicative]: JWTAlgebra[F] = new JWTAlgebra[F] {
    override def validate(jwtAlgorithm: JwtAlgorithm, token: JWTToken, secret: String)(implicit
                                                                                       F: ConcurrentEffect[F])
    : F[Boolean] = {
      Jwt.decode(token.token, secret, Seq(jwtAlgorithm.asInstanceOf[JwtHmacAlgorithm])).toOption match {
        case Some(_) => true.pure[F]
        case None => false.pure[F]
      }
    }

    override def createToken(jwtAlgorithm: JwtAlgorithm, secretKey: String, expirationInSeconds: Long,
                             claims: Map[String, String])(implicit F: ConcurrentEffect[F]): F[String] = {
      F.delay {
        implicit val clock: Clock = Clock.systemDefaultZone()

        Jwt.encode(JwtClaim({
          ""
        }).issuedNow.expiresIn(expirationInSeconds), secretKey, jwtAlgorithm)
      }
    }
  }

  def refreshInter[F[_] : Applicative]: JWTAlgebra[F] = new JWTAlgebra[F] {
    override def validate(jwtAlgorithm: JwtAlgorithm, token: JWTToken, secret: String)(implicit
                                                                                       F: ConcurrentEffect[F])
    : F[Boolean] = {
      Jwt.decode(token.token, secret, Seq(jwtAlgorithm.asInstanceOf[JwtHmacAlgorithm])) match {
        case Failure(exception) =>
          println(token.token)
          println(exception)
          false.pure[F]
        case Success(_) => true.pure[F]
      }
    }

    override def createToken(jwtAlgorithm: JwtAlgorithm, secretKey: String, expirationInSeconds: Long,
                             claims: Map[String, String])(implicit F: ConcurrentEffect[F]): F[String] = {
      F.delay {
        implicit val clock: Clock = Clock.systemDefaultZone()

        Jwt.encode(JwtClaim({
          ""
        }).issuedNow.expiresIn(expirationInSeconds), secretKey, jwtAlgorithm)
      }
    }
  }
}