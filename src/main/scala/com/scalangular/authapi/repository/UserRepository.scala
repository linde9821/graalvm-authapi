package com.scalangular.authapi.repository

import cats.Applicative
import cats.implicits.catsSyntaxApplicativeId

trait UserRepository[F[_]] {
  def isValid(username: String, password: String): F[Boolean]

  def getClaims(username: String): F[Map[String, String]]
}

object UserRepository {
  def testInter[F[_] : Applicative]: UserRepository[F] = new UserRepository[F] {
    override def isValid(username: String, password: String): F[Boolean] = {
      (username == "username" && password == "password").pure[F]
    }

    override def getClaims(username: String): F[Map[String, String]] = {
      Map[String, String](("username", username)).pure[F]
    }
  }
}