ARG GRAAL_VERSION=20.3.0-java11
FROM oracle/graalvm-ce:${GRAAL_VERSION}
WORKDIR /opt/native-image
RUN gu install native-image
ENTRYPOINT ["native-image"]